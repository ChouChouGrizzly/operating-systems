import java.util.Scanner;

/**
 * Class DiningPhilosophers
 * The main starter.
 *
 * @author Serguei A. Mokhov, mokhov@cs.concordia.ca
 * @student Sasa Zhang (Student ID: 25117151)
 */

public class DiningPhilosophers
{
	/*
	 * ------------
	 * Data members
	 * ------------
	 */

	/**
	 * This default may be overridden from the command line
	 */
	public static final int DEFAULT_NUMBER_OF_PHILOSOPHERS = 4;

	/**
	 * Dining "iterations" per philosopher thread
	 * while they are socializing there
	 */
	public static final int DINING_STEPS = 10;

	/**
	 * Our shared monitor for the philosophers to consult
	 */
	public static Monitor soMonitor = null;
	
	/**
	 * Task 3: Variable Number of Philosophers
	 * String variable used to store user entered input.
	 */
	public static String userInput;

	/*
	 * -------
	 * Methods
	 * -------
	 */

	/**
	 * Main system starts up right here
	 */
	@SuppressWarnings("resource")
	public static void main(String[] argv)
	{
		try
		{
			/*
			 * TODO:
			 * Should be settable from the command line
			 * or the default if no arguments supplied.
			 */
			// Variable Number of Philosophers
			// Take user input and use it to initialize the number of philosophers; 
			// Otherwise, use the default number of philosophers.
			Scanner dataIn = new Scanner(System.in);
			System.out.print("Please enter the number of philosophers: ");
			userInput = dataIn.nextLine();
			int iPhilosophers;
			
			if (userInput.isEmpty()) 
			{
				iPhilosophers = DEFAULT_NUMBER_OF_PHILOSOPHERS;
			}
			else if(Integer.parseInt(userInput) <= 0)
			{
				throw new NumberFormatException();
			}
			else 
			{
				// To extract an int value from a character string.
				iPhilosophers = Integer.parseInt(userInput);
			}
				

			// Make the monitor aware of how many philosophers there are
			soMonitor = new Monitor(iPhilosophers);

			// Space for all the philosophers
			Philosopher aoPhilosophers[] = new Philosopher[iPhilosophers];
			
			System.out.println
			(
				iPhilosophers +
				" philosopher(s) came in for a dinner."
			);

			// Let 'em sit down
			for(int j = 0; j < iPhilosophers; j++)
			{
				aoPhilosophers[j] = new Philosopher();
				aoPhilosophers[j].start();
			}

			// Main waits for all its children to die...
			// I mean, philosophers to finish their dinner.
			for(int j = 0; j < iPhilosophers; j++)
				aoPhilosophers[j].join();

			System.out.println("All philosophers have left. System terminates normally.");
			
			// Close the Scanner object.
			dataIn.close();
		}
		
		catch(InterruptedException e)
		{
			System.err.println("main():");
			reportException(e);
			System.exit(1);
		}
		
		// Variable Number of Philosophers
		// If the user input is not a positive integer, report this error to the user
		// Print the usage information as given in the assignment requirements.
		catch(NumberFormatException e) 
		{
			System.err.println("%java DiningPhilosophers " + userInput);
			System.err.println("\"" + userInput + "\"" + " is not a positive decimal integer\n");
			System.err.println("Usage: java DiningPhilosophers [NUMBER_OF_PHILOSOPHERS]\n%");
		}
		
	} // main()

	/**
	 * Outputs exception information to STDERR
	 * @param poException Exception object to dump to STDERR
	 */
	public static void reportException(Exception poException)
	{
		System.err.println("Caught exception : " + poException.getClass().getName());
		System.err.println("Message          : " + poException.getMessage());
		System.err.println("Stack Trace      : ");
		poException.printStackTrace(System.err);
	}
}

// EOF
