import java.util.ArrayList;

/**
 * Class Monitor
 * To synchronize dining philosophers.
 *
 * @author Serguei A. Mokhov, mokhov@cs.concordia.ca
 * @student Sasa Zhang (Student ID: 25117151)
 */

public class Monitor
{
	/*
	 * ------------
	 * Data members
	 * ------------
	 */
	
	/*
	 * Indicate the number of chopsticks
	 */
	int numOfChopsticks;
	
	/*
	 * Indicate if a chopstick is available or not
	 * true: is unavailable to be picked up
	 * false: is available to be picked up
	 */
	private boolean[] isChopsticksInUse;
	
	/*
	 * Indicate if a philosopher could talk or not
	 * true: no one is currently talking
	 * false: somebody is talking
	 */
	private boolean able2Talk;
	
	/*
	 * Indicate who are waiting to eat
	 * once finish eating, the philosopher will be removed from the queue
	 * the queue will be emptied once every philosopher finishes eating
	 */
	private ArrayList<Integer> eatingQueue = new ArrayList<>();
	
	/*
	 * Indicate the states of philosophers
	 */
	enum states {THINKING, HUNGRY, EATING, WAITING, TALKING}
	
	/*
	 * A states array to indicate the states of all philosophers
	 */
	private states[] philosopherStates;


	/**
	 * Constructor
	 */
	public Monitor(int piNumberOfPhilosophers)
	{
		// TODO: set appropriate number of chopsticks based on the # of philosophers
		this.numOfChopsticks = piNumberOfPhilosophers;
		isChopsticksInUse = new boolean[numOfChopsticks];
		
		// Any ONE of philosophers could talk
		// as no one is talking nor eating at initialization.
		able2Talk= true;

		// At initialization, every chopstick is available to be picked up
		for(int i = 0; i < numOfChopsticks; i++)
		{
			isChopsticksInUse[i] = false;	
		}
		
		// At initialization, every philosopher could be placed in the eating queue.
		for(int i = 0; i < numOfChopsticks; i++)
		{
			eatingQueue.add(i, i+1);
		}
		
		// At initialization, every philosopher is in the state of THINKING
		philosopherStates = new states[piNumberOfPhilosophers];
		for(int i = 0; i < numOfChopsticks; i++)
		{
			philosopherStates[i] = states.THINKING;
		}
	}

	/*
	 * -------------------------------
	 * User-defined monitor procedures
	 * -------------------------------
	 */

	/**
	 * Grants request (returns) to eat when both chopsticks/forks are available.
	 * Else forces the philosopher to wait()
	 */
	public synchronized void pickUp(final int piTID)
	{
		try 
		{
			while(true)
			{
				// If the calling philosopher is in the queue then he could eat
				if(eatingQueue.contains(piTID))
				{
					// If both chopsticks are available then ready to be picked up, otherwise need to wait.
					if(!isChopsticksInUse[piTID - 1] && !isChopsticksInUse[piTID % numOfChopsticks])
					{
						// Pick up two chopsticks and change the state to EATING
						isChopsticksInUse[piTID - 1] = true;
						isChopsticksInUse[piTID % numOfChopsticks] = true;
						philosopherStates[piTID - 1] = states.EATING;
						
						//Remove the calling philosopher from eating queue
						for(int i = 0; i < eatingQueue.size(); i++)
						{
							if(eatingQueue.get(i) == piTID)
								eatingQueue.remove(i);
						}
						break;
					}
					else
					{
						// If both chopsticks are not available, the calling philosopher needs to wait.
						philosopherStates[piTID - 1] = states.HUNGRY;
						wait();
					}
				}
				// If every philosopher finishes eating, then start a new eating round.
				else if (eatingQueue.isEmpty())
				{
					for(int i = 0; i < numOfChopsticks; i++)
					{
						eatingQueue.add(i, i+1);
					}
					continue;
				}
				// If the calling philosopher has already eaten, then he has to wait for others to finish eating 
				// to avoid starvation 
				else
				{
					philosopherStates[piTID - 1] = states.WAITING;
					wait();
				}
			} // end of while loop
		}
		catch(InterruptedException e)
		{
			e.printStackTrace();
		}
	}

	/**
	 * When a given philosopher's done eating, they put the chopstiks/forks down
	 * and let others know they are available.
	 */
	public synchronized void putDown(final int piTID)
	{
			// the calling philosopher puts down the chopsticks
			isChopsticksInUse[piTID - 1] = false;
			isChopsticksInUse[piTID % numOfChopsticks] = false;
			
			// make the chopsticks available to other philosophers
			philosopherStates[piTID - 1] = states.THINKING;
			notifyAll();
	}

	/**
	 * Only one philosopher at a time is allowed to philosophy
	 * (while she is not eating).
	 */
	public synchronized void requestTalk(final int piTID)
	{
		try
		{
			// If someone else is talking, the calling philosopher needs to wait.
			while(true)
			{
				// If the calling philosopher is not eating and nobody is talking
				// then the calling philosopher could talk
				if(able2Talk && philosopherStates[piTID - 1] != states.EATING)
				{
					able2Talk = false;
					philosopherStates[piTID - 1] = states.TALKING;
					break;
				}
				// If somebody is talking, the calling philosopher needs to wait.
				else
				{
					philosopherStates[piTID - 1] = states.WAITING;
					wait();
				}
			}
		}

		catch(InterruptedException e)
		{
			e.printStackTrace();
		}
	}

	/**
	 * When one philosopher is done talking stuff, others
	 * can feel free to start talking.
	 */
	public synchronized void endTalk(final int piTID)
	{
		able2Talk = true;
		philosopherStates[piTID - 1] = states.THINKING;
		notifyAll();
	}
}

// EOF
